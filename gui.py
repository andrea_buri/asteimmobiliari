import wx
import sys

APPNAME = "EstrazioneAste"
MULTIPAGE_MSG = "EstrazioneAste has detected a url which spans over multiple pages. Do you want to scrape all the pages?"
RUNNING_MSG = "EstrazioneAste is still running. Do you want to export partial data?"
NODATA_MSG = "No data has been scraped"
EXPORTED_MSG = "Data has been exported!"

import threading
import time
from datetime import datetime
import os

from util import resource_path
from scrape import Scraper


class MyPanel(wx.Panel):
    def __init__(self,parent):
        super(MyPanel,self).__init__(parent)
                
        self.setControllers()
        self.doLayout()
        self.parent = parent

    def doLayout(self):

        hsizer = wx.BoxSizer()
        hsizer.AddStretchSpacer(30)

        
        main = wx.BoxSizer(wx.VERTICAL)
        main.SetMinSize(1000,100)
        #input row
        inputRow = wx.BoxSizer(wx.HORIZONTAL)
        # inputRow.Add(self._chLabel, 0,
        #     wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 10)
        # inputRow.Add(self._url_ctrl, 5,
        #     wx.EXPAND|wx.ALIGN_CENTER_VERTICAL)
        inputRow.Add(self._scrape_button, 1,
            wx.EXPAND|wx.ALIGN_CENTER_VERTICAL)
        main.AddSpacer(10)
        main.Add(inputRow, 0, wx.EXPAND|wx.ALL,5)

        line = wx.StaticLine(self)
        main.AddSpacer(10)
        main.Add(line,flag=wx.EXPAND|wx.BOTTOM,border=10)
        main.AddSpacer(10)

        # log = wx.Panel()
        # logSizer = wx.BoxSizer(wx.VERTICAL)
        # logText = wx.StaticText(logSizer, -1, style = wx.ALIGN_LEFT | wx.ST_ELLIPSIZE_MIDDLE)
        # logText.SetLabel("AB"*300)
        logRow = wx.BoxSizer(wx.HORIZONTAL)
        logRow.AddSpacer(10) 
        
        logRow.Add(self._log_text)

        logRow.AddSpacer(10)

        main.Add(logRow)

        main.AddSpacer(10)
        # exportRow = wx.BoxSizer(wx.HORIZONTAL)
        # exportRow.AddSpacer(10)
        # exportRow.Add(self._export_button,1,wx.EXPAND|wx.ALIGN_RIGHT)
        # main.Add(exportRow)

        hsizer.Add(main, flag=wx.ALIGN_TOP)
        hsizer.AddStretchSpacer(30)
        self.SetSizer(hsizer)


    def setControllers(self):
        # self._chLabel = wx.StaticText(self,
        #                                 label="URL")
        # self._url_ctrl = wx.TextCtrl(self)
        # self._url_ctrl.SetValue("")
        # self._url_ctrl.SetHint("Inserire origine estrazione")
        
        self._scrape_button = wx.Button(self, 
                                        label="Start")
        self._scrape_button.SetBackgroundColour(wx.Colour(30, 90, 240))
        self._scrape_button.SetForegroundColour(wx.Colour(255, 255, 255))
        self._scrape_button.Bind(wx.EVT_BUTTON, self.onScrape)

        # self._export_button = wx.Button(self,
        #                                 label="Esporta")
        # self._export_button.SetBackgroundColour(wx.Colour(30, 90, 240))
        # self._export_button.SetForegroundColour(wx.Colour(255, 255, 255))
        # self._export_button.Bind(wx.EVT_BUTTON, self.onExport)

        self._log_text = wx.TextCtrl(self, size=(1000,200), style=wx.TE_MULTILINE| wx.EXPAND | wx.TE_READONLY)

    def reinit(self):
        #scrape reinit
        self.parent.statusbar.SetStatusText("")
        self._log_text.SetValue("")
        # self._url_ctrl.SetValue("")

    def onPause(self, event):
        #switch btn
        #scrape pause
        self.toggleBtn()
        # self.scraper.pause_scrape()
        
    
    def toggleBtn(self):
        if self._scrape_button.GetLabel() == "Go":
            self._scrape_button.SetLabel("Pause")
            self._scrape_button.Bind(wx.EVT_BUTTON, self.onPause)
        else:
            self._scrape_button.SetLabel("Go")
            self._scrape_button.Bind(wx.EVT_BUTTON, self.onScrape)

    def onScrape(self, event):
        #scrapefoo = check(url)
        #switch btn
        #for part in scrapefoo:
        #    printlog
        #over
        #switch btn to run
        self.parent.statusbar.SetStatusText("Scraping has started")
        #url = self._url_ctrl.GetLineText(0)
        scraper = Scraper(self)
        scraper.get_whole_list_th("brescia")
        self.parent.statusbar.SetStatusText("Scraping has completed")
        # if not self.scraper.check(url):
        #     return
        # self._log_text.WriteText("{}\n".format(url)*100)
        # if self.scraper.is_multipage(url):
        #     result = wx.MessageBox(MULTIPAGE_MSG, "Multi page scraping", wx.YES_NO)
        #     if result == wx.YES:
        #         print ("YES")
        #         self.scraper.setMPRequest()
        #     else:
        #         print ("NO")
        
        self.toggleBtn()
        # self.scraper.start_scrape()

    def doPrint(self,txt):
        self._log_text.WriteText(txt)
    def doAlert(self,text):
        wx.MessageBox(text)


    # def doExport(self):
    #     self.reporter.write_rows(self.scraper.shared['data'])

class MyFrame(wx.Frame):
    def __init__(self,parent,title=""):
        super(MyFrame,self).__init__(parent,title=title,size=wx.Size(400, 400))
        self.panel = MyPanel(self)
        self.initUI() 
        self.statusbar = self.CreateStatusBar(1)
        self.SetIcon(wx.Icon(resource_path(".\\images\\scraper.png")))

    def initUI(self):
        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        newItem = wx.MenuItem(fileMenu,wx.ID_NEW,kind=wx.ITEM_NORMAL)
        fileMenu.Append(newItem)

        quit = wx.MenuItem(fileMenu, wx.ID_EXIT, '&Quit\tCtrl+Q')
    
        fileMenu.Append(quit)
        menubar.Append(fileMenu, '&File')
        self.SetMenuBar(menubar)
        
        # self.text = wx.TextCtrl(self,-1, style = wx.EXPAND|wx.TE_MULTILINE) 
        self.Bind(wx.EVT_MENU, self.menuhandler) 

        self.Show(True)


    def menuhandler(self,event):
        id = event.GetId()
        if id == wx.ID_EXIT:
            try:
                pass#self.panel.scraper.driver.driver.quit()
            except:
                pass
            sys.exit()
        elif id == wx.ID_NEW:
            self.panel.reinit()

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, title=APPNAME)
        self.frame.Show()
        self.frame.Bind(wx.EVT_CLOSE, self.closedWindowEvt)
        return True

    def closedWindowEvt(self, event):
        print ("QUITTING")
        try:
            pass#self.frame.panel.scraper.driver.driver.quit()
        except:
            print ("NO driver")
        sys.exit()
    # def OnFrameShow(self, event):
    #     theFrame = event.EventObject
    #     print("Event:{}".format(theFrame.Title))
    #     event.Skip()

if __name__=="__main__":
    app = MyApp(False)
    app.MainLoop()