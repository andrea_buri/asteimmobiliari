HTTP/1.1 200 OK
X-AspNetMvc-Version: 5.2
Content-Length: 84577
Cache-Control: private
Content-Type: text/html; charset=utf-8
Date: Mon, 03 Jun 2019 19:37:20 GMT
Set-Cookie: ASP.NET_SessionId=uogmukbpbtk33zcqhkwgwn1z; path=/; HttpOnly
Server: Microsoft-IIS/8.5
X-AspNet-Version: 4.0.30319
X-Powered-By: ASP.NET

<!-- Comment to test automatic release with tfs -->
<!DOCTYPE html>
<html>
<head>
    <!-- Basic Page Needs
    ================================================== -->
        <title>Vendita all&#39;asta Appartamento, garage o autorimessa - Carpenedolo (Brescia) - Astegiudiziarie.it</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, pinch-to-zoom=no" />
    <meta name="description" content="Appartamento a piano secondo con balcone.  Autorimessa a piano terra.Competono le proporzionali quote delle parti comuni. Via Lorenzo Forleo - Carpenedolo (BS). Vendita senza incanto. Prezzo base: -." />
    <meta name="keywords" content="asta giudiziaria appartamento garage o autorimessa carpenedolo, vendita appartamento garage o autorimessa all&#39;asta carpenedolo, appartamento garage o autorimessa all&#39;asta tribunale di brescia" />


    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-30294805-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-811785906"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-811785906');
        </script>
        <!-- Google Adwords -->

    <!-- Event snippet for Contatto AG conversion page
    In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
    <script>
        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof (url) != 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-811785906/gjFeCN6TmoABELK9i4MD',
                'event_callback': callback
            });
            return false;
        }
    </script>
        <!-- End Google Adwords -->

    <!-- Facebook Pixel Code  -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
            n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '329963090860687');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=329963090860687&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code  -->


    <!-- META
    ================================================== -->

    <!-- BEGIN SOCIAL METATAG -->
    <meta property="fb:app_id" content="281425342389455" />

    <!-- The card type, which will be one of "summary", "summary_large_image", "app", or "player" -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@astegiudiziarie.it" />
    <meta name="twitter:creator" content="@AsteGiudiziari" />
    

    <meta property="og:url" content="https://www.astegiudiziarie.it/vendita-asta-appartamento-carpenedolo-via-lorenzo-forleo-1645332" />
    <meta property="og:title" content="Vendita all&#39;asta Appartamento, garage o autorimessa - Carpenedolo (Brescia)" />
    <meta property="og:description" content="Appartamento a piano secondo con balcone.  Autorimessa a piano terra.Competono le proporzionali quote delle parti comuni. Via Lorenzo Forleo - Carpenedolo (BS). Vendita senza incanto. Prezzo base: -." />
    <meta property="og:image" content="https://www.astegiudiziarie.it/images/placeholders/appartamento.png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:type" content="website" />
    <!-- END SOCIAL METATAG -->

    <link rel="icon" type="image/png" href="/favicon.png" />
    <link href="/css/autenticazione/login.css" rel="stylesheet" />

    <!-- CSS ================================================== -->

    <link href="/css/jquery-ui.css" rel="stylesheet"/>
<link href="/css/jquery-ui.theme.css" rel="stylesheet"/>

    <link href="/css/style.css" rel="stylesheet"/>
<link href="/css/spacing.css" rel="stylesheet"/>
<link href="/css/footer.css" rel="stylesheet"/>
<link href="/css/colors/main.css" rel="stylesheet"/>
<link href="/css/jquery.smartbanner.css" rel="stylesheet"/>


    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="/css/minimappa-tiles.css" />
    <link rel="stylesheet" href="/css/slider-acution-detail.css" />
    <link rel="stylesheet" href="/css/auction.css" />
    <link rel="stylesheet" href="/css/style-library.css" />
    <link rel="stylesheet" href="/css/astegiudiziarie.css" />
    <link rel="stylesheet" href="/css/owl.carousel.css" />
    <link rel="stylesheet" href="/css/owl.transitions.css" />
    <link rel="stylesheet" href="/css/flag-icon.min.css" />
        <link rel="stylesheet" href="/css/scheda_del_bene.css" />

    <link rel="stylesheet" href="/css/alert.css" />
    <link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />

    <style>
        ul {
            list-style-type: none;
        }

            ul li, ol li {
                font-family: inherit;
                font-size: 14px;
                color: #515157;
                margin-bottom: 5px;
            }
    </style>
    <!-- END PAGE LEVEL STYLES -->
    <link href="/css/custom.css" rel="stylesheet" />






        <script type="text/javascript">
            var graphData = [];
        </script>
</head>
<body>
    <!-- Wrapper -->
    <div id="wrapper">

<!-- Header Container  ================================================== -->
<header id="header-container" class="header-style-small">
    <!-- Header -->
    <div id="header">
        <div class="container no-padding padding-bottom-20" id="header-container-div">
            <!-- Left Side Content -->
            <div class="centered">
                <!-- Main Navigation -->
                <nav id="navigation" class="style-3">
                    <a href="/" class="sticky-logo">
                        <img src="/images/loghi/ag100/bianco.png" alt="Aste Giudiziarie" />
                    </a>
                    <div class="menu-trigger-div">
                        <em class="fa fa-lg fa-reorder menu-trigger"></em>
                    </div>
                    <ul id="responsive">
                            <li class="visible-xs-inline logo-panel">
        <a class="sidebar-logo" href="/">
            <img src="/images/loghi/ag100/colori.png" alt="Aste Giudiziarie" />
        </a>
        <a class="sidebar-collapse" href="javascript:void(0);">
            <i class="fa fa-times"></i>
        </a>
    </li>

    <li>
        <a class="submenu" href="javascript:;">Ricerche</a>
        <ul>
            <li><a href="/">Immobili</a></li>
            <li><a href="/Mobili">Mobili</a></li>
            <li><a href="/Immateriali">Immateriali</a></li>
            <li><a href="/Aziende">Aziende</a></li>
            <li><a href="/Calendario">Calendario aste</a></li>
            <li class="hidden-xs"><a href="/Riepiloghi">Panoramica beni</a></li>
            <li><a href="/Atti-procedurali">Atti procedurali</a></li>
        </ul>
    </li>
    <li>
        <a class="submenu" href="javascript:;">Aste giudiziarie</a>
        <ul>
            <li><a href="/Home/IlPortale">Il portale</a></li>
            <li><a href="/Normativa">Normativa</a></li>
            <li><a href="/Home/CosaSonoLeAsteGiudiziarie">Le aste</a></li>
            <li><a href="/Home/Modulistica">Modulistica</a></li>
        </ul>
    </li>
    <li>
        <a class="submenu" href="javascript:;">Professionisti</a>
        <ul>
            <li><a href="https://areariservata.astegiudiziarie.it/login.aspx?ReturnUrl=%2f" target="_blank">Area riservata</a></li>
            <li>
                <a href="javascript:;">Pubblicare un annuncio</a>
                <ul>
                    <li><a href="/Home/ComePubblicareUnAnnuncio">Invio documentazione</a></li>
                    <li><a href="/Home/PagamentiOnline">Fatturazione e pagamenti</a></li>
                    <li><a href="/Home/GestioneEsitiVendite">Gestione esiti vendite</a></li>
                    <li><a href="/Home/PrenotazioneOnlineVisitaImmobile">Gestione online visite</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">Servizi</a>
                <ul>
                    <li><a href="/Home/ServizioPubblicitaVendite">Pubblicit&#224; delle vendite</a></li>
                    <li><a href="/Home/ServizioVenditatelematica">Vendite telematiche</a></li>
                    <li><a href="/Home/ServizioInformatizzazione">Gestione informatica procedure</a></li>
                    <li><a href="/Home/ServizioSupportoUfficiGiudiziari">Supporto agli uffici giudiziari</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a class="submenu" href="javascript:;">La società</a>
        <ul>
            <li><a href="/Home/ChiSiamo">Chi siamo</a></li>
            <li><a href="/Home/LaNostraStoria">La nostra storia</a></li>
            <li><a href="/Home/INostriClienti">I nostri clienti</a></li>
            <li><a href="/lavora-con-noi">Lavora con noi</a></li>
            <li><a href="/Home/Network">Network</a></li>
            <li><a href="/Home/Privacy">Politica di privacy</a></li>
        </ul>
    </li>

    <li><a href="/News">News</a></li>
    <li><a href="/Home/Contatti">Contatti</a></li>
    <li class="hidden-sm"><span class="separator">&#160;</span></li>
    <li class="userMenu"></li>
    <li class="userMenuLogged"></li>
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->
            </div>
            <!-- Left Side Content / End -->
        </div>
    </div>
    <!-- Header / End -->
</header>
<div id="responsive_overlay"></div>
<div class="clearfix"></div>
<!-- Header Container / End -->
        <div id="cookieIFrameContainer" class="hidden"></div>


<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin: 90px auto;">
        <div class="modal-content text-left">
            <div class="modal-header" style="background-color:#00377b">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnCloseLogin">
                    <span aria-hidden="true"><i class="fa fa-times" style="color:white;" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" style="color:white;">ACCEDI O REGISTRATI </h4>
            </div>
            <div class="modal-body">
                <form>
                    
                    <h4>Accedi come utente</h4>
                    <input type="hidden" id="redirectURL" value="" />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">                                
                                <input type="text" class="form-control" id="txtUsername" name="txtUsername" placeholder="EMAIL" required="required" style="margin: 0 0 15px 0;" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">                                
                                <input type="password" id="txtPassword" class="form-control" name="txtPassword" placeholder="PASSWORD" required="required" style="margin: 0 0 15px 0;" />
                            </div>
                        </div>
                        <div class="col-sm-4 text-left">
                            <button type="button" id="btnLogin" name="btnLogin" class="button btn-default">Accedi</button>
                        </div>
                        <div class="col-sm-12">
                            <p class="small margin-top-10">Oppure <a href="/Home/RegisterUser"><strong>Registrati</strong></a> | <a href="/Home/LostPassword"><strong>Recupera password</strong></a></p>
                        </div>
                        <div class="col-md-12">
                            <p class="small" style="margin: 0 0 10px; line-height: 20px;">
                                La registrazione al sito permette l'accesso ai servizi ed alle funzionalità 
                                dei portali di <br /> <strong>Astegiudiziarie.it</strong>, <strong>ReteAste.it</strong> 
                                e all'<strong>App di Aste Giudiziarie</strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-danger" style="padding:10px 15px;" id="loginErrorMsg"></div>                  
                    </div>
                </form>                
                <form style="margin:-15px; padding:15px; border-top: solid 1px #00377b; background-color:#f5f5f5;">
                    <h4>Accedi come professionista</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="small" style="margin:0 0 10px; line-height:20px;">
                                Accedi all'area riservata di astegiudiziarie come professionista. L'area permette di usufruire di una serie di servizi finalizzati alla miglior gestione e monitoraggio delle pubblicazioni richieste.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <a href="https://areariservata.astegiudiziarie.it" class="button btn-info  text-center">Accedi come professionista</a>
                            
                        </div>
                    </div>
                </form>
            </div>            
        </div>
    </div>
</div>

        














<!-- Titlebar Scheda
================================================== -->
<div id="titlebar" class="property-titlebar margin-bottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 title-bar-header-auction">

                <div class="title-bar-auction-left">
                    COD. 2785584
                    
                </div>                
                <div class="auction-check-green"></div>
                <div class="title-bar-auction-right">DA BANDIRE TRA PIU DI 15 GIORNI</div>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">


                <div class="property-title">
                    <h2>                                  

                            APPARTAMENTO, GARAGE O AUTORIMESSA
                    </h2>
                    <span style="word-wrap: break-word; width: 100%;">
                        <a href="#location-title" class="listing-address">
                            <em class="fa fa-map-marker address-icon"></em>
Via Lorenzo Forleo-Carpenedolo (BS)
                        </a>
                    </span>
                </div>


                <div class="property-pricing">
                    <div><span class="property-base">PREZZO BASE</span> 
                             -
                    </div>
                        <div class="sub-price">&nbsp;</div>
                </div>


            </div>


        </div>
    </div>
</div>

<!-- Content
================================================== -->





<div class="container no-media">
    
    <input type="hidden" id="info_idAsta" value="2785584" />

    <input data-val="true" data-val-number="The field idAsta must be a number." data-val-required="The idAsta field is required." id="lotto_idAsta" name="lotto.idAsta" type="hidden" value="2785584" />

    <div class="row margin-bottom-50">
        <div class="col-md-8 mt-xs-100">




<div class="nascondi visible-sm visible-xs responsive-lang margin-top-100">

    <div class="widget margin-bottom-10 block-right mobile-social-share">

        <div id="socialShareMobile" class="btn-group share-group">
            <button data-toggle="dropdown" class="btn btn-info">
                <i class="fa fa-share-alt fa-inverse"></i>
            </button>
            <button href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle share">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <a data-original-title="Twitter" rel="tooltip" href="#" class="btn btn-twitter" data-placement="left" title="Condividi su Twitter">
                        <i class="fa fa-twitter fa-inverse"></i>
                    </a>
                </li>
                <li>
                    <a data-original-title="Facebook" rel="tooltip" href="#" class="btn btn-facebook" data-placement="left" title="Condividi su Facebook">
                        <i class="fa fa-facebook fa-inverse"></i>
                    </a>
                </li>
                <li>
                    <a data-original-title="LinkedIn" rel="tooltip" href="#" class="btn btn-linkedin" data-placement="left" title="Condividi su Linkedin">
                        <i class="fa fa-linkedin fa-inverse"></i>
                    </a>
                </li>
                <li>
                    <a data-original-title="Email" rel="tooltip" href="mailto:?subject=Vendita all&#39;asta Appartamento, garage o autorimessa - Carpenedolo (Brescia)&amp;body=Appartamento a piano secondo con balcone.  Autorimessa a piano terra.Competono le proporzionali quote delle parti comuni.%0D%0Ahttps%3a%2f%2fwww.astegiudiziarie.it%2fvendita-asta-appartamento-carpenedolo-via-lorenzo-forleo-1645332" class="btn btn-mail" data-placement="left" title="Condividi via Email">
                        <i class="fa fa-envelope fa-inverse"></i>
                    </a>
                </li>
                <li>
                </li>

            </ul>
        </div>

        <button class="btn btn-info btn-print margin-right-1" onclick="window.print()"><em class="fa fa-print"></em></button>
        <a href="#" class="btn btn-info btn-follow customhidden" id="btnSeguiAsta2"><em class="fa fa-star-o"></em><span class="hidden-md hidden-xxs"> segui</span></a>
        <a href="#" class="btn btn-info btn-follow customhidden" id="btnAstaSeguita2"><em class="fa fa-star" style="color:#E7D535;"></em><span class="hidden-md hidden-xxs"> Seguita</span></a>
            <a href="#modal-login" class="btn btn-info btn-follow customhidden" id="btnShowLogin2" role="button" data-toggle="modal"><em class="fa fa-star-o"></em><span class="hidden-md hidden-xxs"> segui</span></a>

        
    </div>
    <!-- new social icons -->

        <hr class="hidden-xs hidden-sm" />
</div>
<!-- Widget xs / End -->



        <div id="box" class="nascondi tab-content tab-content-ag not-visible">

        </div>



    <script type="text/javascript">
    </script>


<div class="modal fade" id="bs-error" tabindex="-1" role="dialog" aria-labelledby="bs-error-title">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="formSegnalazioneErrori">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" id="bs-error-title">
                    SEGNALA ALLO STAFF
                </h4>
            </div>
            <form action="#" method="post" id="formInviaSegnalazioneMODAL">
                <div class="modal-body">
                    <p>
                        Segnalaci eventuali errori nella scheda dettagliata del lotto in pubblicità:
                    </p>
                    <input type="hidden" id="mailErrori" value="errori@astegiudiziarie.it" />
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="errore_nome">Nome</label>
                                <input type="text" id="errore_nome" class="form-control" name="Nome"
                                       placeholder="es: Mario" required="required" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="errore_cognome">Cognome</label>
                                <input type="text" id="errore_cognome" class="form-control"
                                       name="Cognome" placeholder="es: Rossi" required="required" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="errore_telefono">Telefono</label>
                                <input type="text" id="errore_telefono" class="form-control"
                                       name="Telefono" placeholder="es: 0586 014854"
                                       required="required" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="errore_tipo">Tipo di errore</label>
                                <select id="errore_tipo"
                                        class="form-control chosen-select-no-single"
                                        required="required" name="TipoErrore">

                                    <option value="1">Indirizzo sbagliato</option>
                                    <option value="2">Posizione mappa sbagliata</option>
                                    <option value="3">Dati non corretti</option>
                                    <option value="4">Numero di telefono non corretto/inesistente</option>
                                    <option value="5">Altro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="errore_email">Email</label>
                                <input type="text" id="errore_email" class="form-control" name="Email"
                                       placeholder="es: info@astegiudiziarie.it" required="required" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="errore_email_2">Conferma Email</label>
                                <input type="text" id="errore_email_2" class="form-control"
                                       name="EmailConferma" placeholder="es: info@astegiudiziarie.it"
                                       required="required" />

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="errore_comunicazione">Note</label>
                                <textarea id="errore_comunicazione" class="form-control"
                                          name="DescrizioneErrore"
                                          placeholder="es: ho provato a contattare il custode telefonicamente, ma il numero risulta inesistente"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="errore_dati_personali">
                                        <input type="checkbox" id="errore_dati_personali"
                                               name="Consenso" required="required" value="true" />
                                        Fornisco il consenso al trattamento dei dati ai sensi del D.lgs n.196/2003
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <br />
                    <div class="row">
                        <div class="col-md-12" style="text-align: -webkit-center;">
                            <div id="recaptcha-demo" class="g-recaptcha" data-sitekey="6Lc6Y0YUAAAAAP0ojTGmwRD2r7etVBiY-va5iS0q"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="esito-segnalazione-errori">

                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="button btn-default" id="btnInviaSegnalazione">Invia</button>
                    <button type="reset" class="button btn-default" data-dismiss="modal">Chiudi</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="bs-errorsent" tabindex="-1" role="dialog" aria-labelledby="bs-error-title">
    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content" id="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" id="bs-error-title">INVIO SEGNALAZIONE</h4>
            </div>
            <div class="modal-body">
                <div class="esito-segnalazione-erroriMODAL">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button btn-default" id="btnInviaNuovaSegnalazioneMODAL" data-toggle="modal" data-target="#bs-error">Invia una nuova segnalazione</button>
                <button type="reset" class="button btn-default" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>
                <!-- FINE BOX -->

            <!-- Widget -->
            <div class="widget visible-sm-block visible-xs-block">
                    <h3 class="margin-bottom-10">Dati relativi alla Vendita</h3>
                    <hr />
                    <div class="row margin-bottom-30">
                            <div class="row" style="margin:0 10px 30px;">

                                <div class="col-xs-4">
                                    <div class="property-single-feature tab-cell-name">Data e ora vendita</div>
                                    <div class="property-single-feature-desc tab-cell-feature">

                                        12/07/2019 ore 12:00
                                    </div>
                                </div>

                                <div class="col-xs-4">
                                    <div class="property-single-feature tab-cell-name">Tipo Vendita</div>
                                    <div class="property-single-feature-desc tab-cell-feature">senza incanto</div>
                                </div>

                                <div class="col-xs-4">
                                    <div class="property-single-feature tab-cell-name">Modalita di vendita</div>
                                    <div class="property-single-feature-desc tab-cell-feature">Presso il venditore</div>
                                </div>

                            </div>
                            <div class="row" style="margin:0 10px 0;">

                                

                                <div class="col-xs-4">
                                    <div class="property-single-feature tab-cell-name">Termine presentazione offerte</div>
                                    <div class="property-single-feature-desc tab-cell-feature">

                                        11/07/2019 ore 11:00
                                    </div>
                                </div>
                            </div>
                    </div>
                        <h3 class="desc-headline side-property-description">Servizi on-line</h3>
                        <div class="row-bottoni">




                                <div class="col-sm-4">
                                    <a class="button fullwidth property-action-small action-after-login" href="javascript:void(0);" data-url-action="/Allegato/Zip/1645332">
                                        <em class="fa fa-file-zip-o fa-lg"></em> ZIP ALLEGATI
                                    </a>
                                </div>



                        </div>


                    <div class="clearfix"></div>




<hr class="small" />
<h3 class="allegati margin-bottom-10 margin-top-20">Allegati</h3>

<div class="rowFiles margin-bottom-20">
                <div class="col-sm-3 col-xs-12">
                    <a href="/Allegato/Avviso-BS-CS-3-2018-DV20190712-1.pdf/1645332" class="sidebar-attachment button" target="_blank">
                        <em class="im fa-lg im-icon-File-Block"></em>
                        AVVISO
                    </a>
            </div>
            <div class="col-sm-3 col-xs-12">
                    <a href="/Allegato/Perizia-BS-CS-3-2018-C-1.pdf/1645332" class="sidebar-attachment button" target="_blank">
                        <em class="im fa-lg im-icon-File-Block"></em>
                        PERIZIA
                    </a>
            </div>
</div>
<div class="clearfix"></div>
<hr class="small" />
                    <!-- Widget -->
                    <!-- Widget / End -->
            </div>
            <!-- Widget -->


            <div class="property-description">
                <!-- Main Features -->
                


<div class="row legal-row">
    <div class="col-md-12 legal-header">
        <span class="im fa-lg im-icon-File-HorizontalText"> </span> Dati relativi al lotto
    </div>
    <div class="col-md-12 legal-row-desc">
        
        <p style="margin-top:10px;">Appartamento a piano secondo con balcone.  Autorimessa a piano terra.Competono le proporzionali quote delle parti comuni.</p>

        <div class="row legal-row-detail">
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Indirizzo</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    Via Lorenzo Forleo, Carpenedolo (BS)
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Lotto</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    1
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Numero beni</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    2
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Genere</div>
                <div class="property-single-feature-desc tab-cell-feature">

                    IMMOBILI

                    
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Categoria</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    IMMOBILI - IMMOBILE RESIDENZIALE
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Valore di stima</div>
                <div class="property-single-feature-desc tab-cell-feature">-</div>
            </div>
        </div>

    </div>
</div>



                
<!-- Details -->

<div class="row legal-row">
    <div class="col-md-12 legal-header">
        <span class="im fa-lg im-icon-File-HorizontalText"> </span> Dati relativi ai beni
    </div>


<!-- Details -->

<div class="col-md-12 legal-row-desc">
    <h4 style="margin-top:20px;">APPARTAMENTO</h4>

    <p style="margin-top:10px;">Appartamento a piano secondo con balcone. </p>

    <div class="row legal-row-detail">
        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Indirizzo</div>
            <div class="property-single-feature-desc tab-cell-feature">
                Via Lorenzo Forleo, Carpenedolo (BS)
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Piano</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Disponibilit&#224;</div>
            <div class="property-single-feature-desc tab-cell-feature">
                Occupato.
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Vani</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>
        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Metri quadri</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Certificazione Energetica</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

    </div>

    <div class="row legal-row-detail">
                <div class="col-sm-12 col-xs-12 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Dati Catastali</div>
                    <div class="property-single-feature-desc tab-cell-feature">


                        -
                    </div>
                </div>

    </div>
</div>

            <hr style="margin-bottom: 10px; border: 1px solid #eee" />

<!-- Details -->

<div class="col-md-12 legal-row-desc">
    <h4 style="margin-top:20px;">GARAGE O AUTORIMESSA</h4>

    <p style="margin-top:10px;">Autorimessa a piano terra.  Competono le proporzionali quote sulle parti comuni.</p>

    <div class="row legal-row-detail">
        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Indirizzo</div>
            <div class="property-single-feature-desc tab-cell-feature">
                Via Lorenzo Forleo, Carpenedolo (BS)
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Piano</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Disponibilit&#224;</div>
            <div class="property-single-feature-desc tab-cell-feature">
                Occupata.
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Vani</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>
        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Metri quadri</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

        <div class="col-sm-4 col-xs-6 col-xxs-12">
            <div class="property-single-feature tab-cell-name">Certificazione Energetica</div>
            <div class="property-single-feature-desc tab-cell-feature">
                -
            </div>
        </div>

    </div>

    <div class="row legal-row-detail">
                <div class="col-sm-12 col-xs-12 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Dati Catastali</div>
                    <div class="property-single-feature-desc tab-cell-feature">


                        -
                    </div>
                </div>

    </div>
</div>


</div>



                


<div class="row legal-row">
    <div class="col-md-12 legal-header">
        <span class="im fa-lg im-icon-File-HorizontalText"> </span> Dati relativi alla Vendita
    </div>

    <div class="col-md-12 legal-row-desc">
        <div class="row legal-row-detail">
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Data e ora vendita</div>

                <div class="property-single-feature-desc tab-cell-feature">12/07/2019 ore 12:00</div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Tipo Vendita</div>
                <div class="property-single-feature-desc tab-cell-feature">senza incanto</div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Modalita vendita</div>
                <div class="property-single-feature-desc tab-cell-feature">Presso il venditore</div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">LUOGO DELLA VENDITA</div>
                <div class="property-single-feature-desc tab-cell-feature">Via Ugo La Malfa, 4 - Brescia</div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">
                    
                    Luogo presentazione offerta
                </div>
                <div class="property-single-feature-desc tab-cell-feature">
                    -
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">
                    Termine presentazione offerte
                </div>
                <div class="property-single-feature-desc tab-cell-feature">

                    11/07/2019 ore 11:00
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Prezzo Base</div>
                <div class="property-single-feature-desc tab-cell-feature">-</div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Offerta minima</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    -
                </div>
            </div>

            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Rialzo minimo in caso di gara</div>
                <div class="property-single-feature-desc tab-cell-feature">
€ 700,00
                </div>
            </div>


            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Deposito cauzionale</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Deposito in conto spese</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    
                </div>
            </div>




            


        </div>
    </div>

</div>



                

<div class="row legal-row">
    <div class="col-md-12 legal-header">
        <span class="im fa-lg im-icon-File-HorizontalText"> </span> Dettaglio procedura e contatti
    </div>

    <div class="col-md-12 legal-row-desc">
        <div class="row legal-row-detail">
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">
                    Tribunale
                </div>
                <div class="property-single-feature-desc tab-cell-feature">
                        <a href="http://www.tribunale.brescia.giustizia.it" target="_blank" title="Vai al sito del Tribunale">Brescia</a>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Tipo procedura</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    
Crisi da sovraindebitamento                </div>
            </div>
            <div class="col-sm-4 col-xs-6 col-xxs-12">
                <div class="property-single-feature tab-cell-name">Ruolo</div>
                <div class="property-single-feature-desc tab-cell-feature">
                    3
                         / 2018
                                    </div>
            </div>

                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">
                        Giudice
                    </div>
                    <div class="property-single-feature-desc tab-cell-feature">
                        Agnese Vincenza
                                            </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Recapiti</div>

                    <div class="property-single-feature-desc tab-cell-feature">
                        -
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Email</div>
                    <div class="property-single-feature-desc tab-cell-feature">
                            -
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">
                        Professionista delegato
                    </div>
                    <div class="property-single-feature-desc tab-cell-feature">
                        Iannasso Francesco Saverio
                            <div style="font-size: 11px;">(Procede alle operazioni di vendita)</div>
                                            </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Recapiti</div>

                    <div class="property-single-feature-desc tab-cell-feature">
                        -
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Email</div>
                    <div class="property-single-feature-desc tab-cell-feature">
                                    <a href="mailto:"></a>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">
                        Liquidatore
                    </div>
                    <div class="property-single-feature-desc tab-cell-feature">
                        Zola Annarita
                                            </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Recapiti</div>

                    <div class="property-single-feature-desc tab-cell-feature">
                        Tel. 030 21 90 209<br />Fax 030 21 91 522
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6 col-xxs-12">
                    <div class="property-single-feature tab-cell-name">Email</div>
                    <div class="property-single-feature-desc tab-cell-feature">
                                    <a href="mailto:annaritazola@studiovielmi.net">annaritazola@studiovielmi...</a>
                    </div>
                </div>
        </div>

    </div>
</div>




                









                <div class="row legal-detail">
                    <div class="col-sm-12">
                        <hr />
                        <div class="published">
                                    <a href="javascript:;" data-toggle="modal" data-target="#bs-error">Segnala allo staff</a>
                                 -

                            Scheda <strong>COD. 2785584</strong> -
                            Pubblicata dal 27/05/2019
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sidebar -->
        <div class="col-lg-4 col-md-4">
            <div class="sidebar sticky right">
                <div class="nascondi hidden-xs">

                    <div class="widget margin-bottom-10 block-right mobile-social-share">

                        <div id="socialShare" class="btn-group share-group">
                            <button data-toggle="dropdown" class="btn btn-info">
                                <i class="fa fa-share-alt fa-inverse"></i>
                            </button>
                            <button href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle share">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a data-original-title="Twitter" rel="tooltip" href="#" class="btn btn-twitter" data-placement="left" title="Condividi su Twitter">
                                        <i class="fa fa-twitter fa-inverse"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-original-title="Facebook" rel="tooltip" href="#" class="btn btn-facebook" data-placement="left" title="Condividi su Facebook">
                                        <i class="fa fa-facebook fa-inverse"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-original-title="LinkedIn" rel="tooltip" href="#" class="btn btn-linkedin" data-placement="left" title="Condividi su Linkedin">
                                        <i class="fa fa-linkedin fa-inverse"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-original-title="Email" rel="tooltip" href="mailto:?subject=Vendita all&#39;asta Appartamento, garage o autorimessa - Carpenedolo (Brescia)&amp;body=Appartamento a piano secondo con balcone.  Autorimessa a piano terra.Competono le proporzionali quote delle parti comuni.%0D%0Ahttps%3a%2f%2fwww.astegiudiziarie.it%2fvendita-asta-appartamento-carpenedolo-via-lorenzo-forleo-1645332" class="btn btn-mail" data-placement="left" title="Condividi via Email">
                                        <i class="fa fa-envelope fa-inverse"></i>
                                    </a>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>

                        <button class="btn btn-info btn-print margin-right-1" onclick="window.print()"><em class="fa fa-print"></em></button>
                        <a href="#" class="btn btn-info btn-follow" id="btnSeguiAsta1"><em class="fa fa-star-o"></em><span class="hidden-md hidden-xxs"> segui</span></a>
                        <a href="#" class="btn btn-info btn-follow customhidden" id="btnAstaSeguita1"><em class="fa fa-star" style="color: #E7D535;"></em><span class="hidden-md hidden-xxs"> Seguita</span></a>
                            <a href="#modal-login" class="btn btn-info btn-follow customhidden" id="btnShowLogin1" role="button" data-toggle="modal"><em class="fa fa-star-o"></em><span class="hidden-md hidden-xxs"> segui</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- new social icons -->

                        <hr class="hidden-xs hidden-sm" />
                </div>

                    <!-- Widget -->
                    <div class="widget hidden-sm hidden-xs">
                        <h3 class="desc-headline side-property-description">Dati relativi alla Vendita</h3>
                            <div class="property-single-feature tab-cell-name">Data e ora vendita</div>
                            <div class="property-single-feature-desc tab-cell-feature">

                                12/07/2019 ore 12:00
                            </div>
                            <hr class="small" />
                            <div class="property-single-feature tab-cell-name">Tipo Vendita</div>
                            <div class="property-single-feature-desc tab-cell-feature">senza incanto</div>
                            <hr class="small" />
                            <div class="property-single-feature tab-cell-name">Modalita vendita</div>
                            <div class="property-single-feature-desc tab-cell-feature">Presso il venditore</div>
                            <hr class="small" />
                            <div class="property-single-feature tab-cell-name">Termine presentazione offerte</div>
                            <div class="property-single-feature-desc tab-cell-feature line-height-20">

                                11/07/2019 ore 11:00
                            </div>

                    </div>



<!-- Widget -->
<div class="widget hidden-sm hidden-xs">
    <h3 class="desc-headline side-property-description">Allegati</h3>
                    <a href="/Allegato/Avviso-BS-CS-3-2018-DV20190712-1.pdf/1645332" class="sidebar-attachment button" target="_blank">
                    <em class="im fa-lg im-icon-File-Block"></em>
                    AVVISO
                </a>
                <a href="/Allegato/Perizia-BS-CS-3-2018-C-1.pdf/1645332" class="sidebar-attachment button" target="_blank">
                    <em class="im fa-lg im-icon-File-Block"></em>
                    PERIZIA
                </a>
</div>
<!-- Widget / End -->
                    <!-- Widget -->
                        <div class="widget hidden-sm hidden-xs">
                            <h3 class="desc-headline side-property-description">Servizi on-line</h3>
                            <div class="widget margin-bottom-10 hidden-sm hidden-xs">






                                    <a class="button fullwidth property-action-small action-after-login" href="javascript:void(0);" data-url-action="/Allegato/Zip/1645332">
                                        <em class="fa fa-file-zip-o fa-lg"></em> ZIP ALLEGATI
                                    </a>




                            </div>
                        </div>
                    <!-- Widget / End -->



<!-- Nav tabs -->
    <div class="widget hidden-sm hidden-xs">
        
        <h3 class="desc-headline side-property-description">Vendite gi&#224; effettuate</h3>
        
        <div class="widget margin-bottom-10" style="z-index: 5;">
            <!-- TODO AG: Se c'è il grafico, questo deve essere indicato come TAB ATTIVO altrimenti il grafico sballa -->
            <ul class="nascondi nav tabs-nav chartmenu margin-right-0" role="tablist">
                <li class="active">
                    <a href="#linechart-container"><em class="im fa-lg im-icon-Line-Chart"></em> Andamento</a>
                </li>
                <li class="margin-right-0" style="float:right">
                    <a href="#table-container"><em class="im fa-lg im-icon-File-HorizontalText"></em> Versione Tabellare</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="nascondi tab-content active" id="linechart-container">
                    <div id="linechart_material" class="hidden-xs hidden-md hidden-1240"></div>
                    <div id="linechart_material_2" class="visible-xs-block visible-md-block visible-1240-block"></div>
                    <div class="legend">
                        <ul>
                            
                            <li style="display: none;"><em class="fa fa-circle a_trattativa_privata" title="a trattativa privata"></em> a trattativa privata</li>
                            <li style="display: none;"><em class="fa fa-circle asincrona_telematica" title="asincrona telematica"></em> asincrona telematica</li>
                            <li style="display: none;"><em class="fa fa-circle asta_privata" title="asta privata"></em> asta privata</li>
                            <li style="display: none;"><em class="fa fa-circle atto_pubblico_notarile" title="atto pubblico notarile"></em> atto pubblico notarile</li>
                            <li style="display: none;"><em class="fa fa-circle competitiva" title="competitiva"></em> competitiva</li>
                            <li style="display: none;"><em class="fa fa-circle con_incanto" title="con incanto"></em> con incanto</li>
                            <li style="display: none;"><em class="fa fa-circle gara_informale" title="gara informale"></em> gara informale</li>
                            <li style="display: none;"><em class="fa fa-circle ignota" title="ignota"></em> ignota</li>
                            <li style="display: none;"><em class="fa fa-circle invito_a_manifestare_interesse" title="invito a manifestare interesse"></em> invito a manifestare interesse</li>
                            <li style="display: none;"><em class="fa fa-circle invito_a_offrire" title="invito a offrire"></em> invito a offrire</li>
                            <li style="display: none;"><em class="fa fa-circle licitazione_privata" title="licitazione privata"></em> licitazione privata</li>
                            <li style="display: none;"><em class="fa fa-circle mezzo_commissionario" title="mezzo commissionario"></em> mezzo commissionario</li>
                            <li style="display: none;"><em class="fa fa-circle offerta_segreta" title="offerta segreta"></em> offerta segreta</li>
                            <li style="display: none;"><em class="fa fa-circle senza_incanto" title="senza incanto"></em> senza incanto</li>
                            <li style="display: none;"><em class="fa fa-circle sincrona" title="sincrona"></em> sincrona</li>
                            <li style="display: none;"><em class="fa fa-circle sincrona_mista" title="sincrona mista"></em> sincrona mista</li>
                            <li style="display: none;"><em class="fa fa-circle sincrona_telematica" title="sincrona telematica"></em> sincrona telematica</li>
                            <li style="display: none;"><em class="fa fa-circle sollecitazione_di_locazione" title="sollecitazione di locazione"></em> sollecitazione di locazione</li>
                            <li style="display: none;"><em class="fa fa-circle udienza_di_assegnazione" title="udienza di assegnazione"></em> udienza di assegnazione</li>
                            <li style="display: none;"><em class="fa fa-circle vendita_da-definire" title="vendita da definire"></em> vendita da definire</li>
                        </ul>
                    </div>
                </div>
                <div role="tabpanel" class="mostra tab-content visible-1240-block"
                     id="table-container">
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Data Vendita</th>
                                    <th class="hidden-xs hidden-sm">Tipologia</th>
                                    <th>Prezzo</th>
                                </tr>
                            </thead>
                            <tbody id="graphDataTable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

            </div>
        </div>
        <!-- Sidebar / End -->
    </div>
    
    
<style>
    #info_privacy {
        margin-left: 17px !important;
        margin-top: 14px !important;
    }
</style>

<div class="modal fade" id="bs-informazioni" tabindex="-1" role="dialog" aria-labelledby="bs-informazioni-title">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="formRichiestaInformazioni">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" id="bs-visita-title">MODULO DI RICHIESTA INFORMAZIONI RIF. A 2785584</h4>
            </div>
            
            <form id="informazioneformMODAL" action="#" method="post">
                <input type="hidden" id="mailInformazioni" value="website@astegiudiziarie.it" />

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">   
                            <div class="form-group alert alert-warning">
                                <div class="padding-bottom-10"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><strong> ATTENZIONE! <u>NON USARE QUESTO MODULO PER RICHIEDERE LA VISITA AI BENI</u></strong></div>
                                Per concordare la visita è necessario consultare gli allegati presenti sulla scheda del bene (in particolare l'avviso laddove disponibile) e contattare il Custode ai recapiti indicati o il Professionista responsabile della procedura.
                            </div>
                        </div>
                    </div>

                    <!-- NOME/COGNOME/EMAIL -->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_nome">Nome</label>
                                <input name="info_nome" type="text" id="info_nome" placeholder="NOME *" required="required" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_cognome">Cognome</label>
                                <input name="info_cognome" type="text" id="info_cognome" placeholder="COGNOME *" required="required" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_email">Email</label>
                                <input name="info_email" type="email" id="info_email" placeholder="EMAIL *" required="required" />
                            </div>
                        </div>

                        <!-- TELEFONO/COMUNE/PROVINCIA -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_phone">Telefono</label>
                                <input name="info_phone" type="text" id="info_telefono" placeholder="TELEFONO" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_comune">Comune</label>
                                <input name="info_comune" type="text" id="info_comune" placeholder="COMUNE" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="info_provincia">Provincia</label>
                                <input name="info_provincia" type="text" id="info_provincia" placeholder="PROVINCIA" />
                            </div>
                        </div>
                        
                        <!-- OGGETTO -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="info_oggetto">Oggetto</label>
                                <input name="info_oggetto" type="text" id="info_oggetto" placeholder="OGGETTO *" required="required"/>
                            </div>
                        </div>

                        <!-- MESSAGGIO -->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="info_comments">Messaggio</label>
                                <textarea name="info_comments" cols="40" rows="3" id="info_comments" placeholder="MESSAGGIO *" spellcheck="true" required="required" class="error"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-left:-30px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="info_privacy">
                                        <input type="checkbox" id="info_privacy" name="info_privacy" value="true" required="required" />
                                        Fornisco il consenso al trattamento dei dati ai sensi del D.lgs n.196/2003
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-12" style="text-align: -webkit-center;">
                            <div id="recaptcha-demo" class="g-recaptcha" data-sitekey="6Lc6Y0YUAAAAAP0ojTGmwRD2r7etVBiY-va5iS0q"></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="esito-segnalazione">

                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="button btn-default" id="btnInviaRichiestaInforamzioni" data-toggle="modal" data-target="#bs-richiestaInfo">Invia</button>
                    <button type="reset" class="button btn-default" data-dismiss="modal">Chiudi</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="bs-info-result" tabindex="-1" role="dialog" aria-labelledby="bs-info-title">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="form-inforichiesta-sent">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" id="bs-error-title">INVIO RICHIESTA INFORMAZIONI</h4>
            </div>
            <div class="modal-body">
                <div class="esito-segnalazioneMODAL">

                </div>
            </div>
            <div class="modal-footer margin-bottom-50">
                <button type="reset" class="button btn-default" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>



</div>



<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer nascondi">
    <!-- Main -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <a href="/"><img src="/images/loghi/ag100/bianco.png" alt="Aste giudiziarie" class="footer-logo" /></a>
            </div>
        </div>
        <hr class="xs15" />
        <div class="row">
            <div class="col-sm-6">
                <ul id="footer-accordion" class="small-footer-links">
                    <li>
                        <a class="submenu h4" href="javascript:;">Ricerche</a>
                        <ul>
                            <li><a class="hoverable" href="/" rel="nofollow">Immobili</a></li>
                            <li><a class="hoverable" href="/Mobili" rel="nofollow">Mobili</a></li>
                            <li><a class="hoverable" href="/Immateriali" rel="nofollow">Immateriali</a></li>
                            <li><a class="hoverable" href="/Aziende" rel="nofollow">Aziende</a></li>
                            <li><a class="hoverable" href="/Calendario" rel="nofollow">Calendario aste</a></li>
                            <li class="hidden-xs" rel="nofollow"><a class="hoverable" href="/Riepiloghi" rel="nofollow">Panoramica beni</a></li>
                            <li><a class="hoverable" href="/Atti-procedurali" rel="nofollow">Atti procedurali</a></li>
                        </ul>
                    </li>
                    <li class="vertical-break">
                        <a class="submenu h4" href="javascript:;">Aste giudiziarie</a>
                        <ul>
                            <li><a href="/Home/IlPortale" rel="nofollow">Il portale</a></li>
                            <li><a href="/Normativa" rel="nofollow">Normativa</a></li>
                            <li><a href="/Home/CosaSonoLeAsteGiudiziarie" rel="nofollow">Le aste</a></li>
                            <li><a href="/Home/Modulistica" rel="nofollow">Modulistica</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="submenu h4" href="javascript:;">Professionisti</a>
                        <ul>
                            <li><a class="hoverable" href="http://areariservata.astegiudiziarie.it/login.aspx?ReturnUrl=%2f">Area riservata</a></li>
                            <li>
                                <a style="cursor:default;" href="javascript:;">Pubblicare annuncio</a>
                                <ul>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/ComePubblicareUnAnnuncio" rel="nofollow">Invio documentazione</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/PagamentiOnline" rel="nofollow">Fatturazione e pagamento</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/GestioneEsitiVendite" rel="nofollow">Gestione esiti vendite</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/PrenotazioneOnlineVisitaImmobile" rel="nofollow">Gestione online visite</a></li>
                                </ul>
                            </li>
                            <li>
                                <a style="cursor:default;" href="javascript:;">Servizi</a>
                                <ul>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/ServizioPubblicitaVendite" rel="nofollow">Pubblicità delle vendite</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/ServizioVenditatelematica" rel="nofollow">Vendite telematiche</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/ServizioInformatizzazione" rel="nofollow">Gestione informatica procedure</a></li>
                                    <li class="lh-18"><a class="fs-12 hoverable" href="/Home/ServizioSupportoUfficiGiudiziari" rel="nofollow">Supporto agli uffici giudiziari</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="clearfix hidden-sm hidden-md hidden-lg"></div>
            <hr class="hidden-sm hidden-md hidden-lg" />

            <div class="col-md-3 col-xs-12 col-sm-3">
                <div class="clearfix"></div>
                <div class="h4">Contatti</div>
                <p class="address-footer">
                    Aste Giudiziarie Inlinea S.p.A. <br />
                    Via Delle Grazie, 5 <br />
                    57125, Livorno (LI) <br />
                    P. IVA 01301540496 <br />
                    Codice Unico Fatturazione: M5UXCR1 <br />
                    REA LI - 116749 <br />
                    Telefono: +39 0586 20141 <br />
                    E-mail: <span><a class="hoverable" href="mailto:info@astegiudiziarie.it">info@astegiudiziarie.it</a></span>
                </p>
                <p class="hidden-xs small-font-footer">
                    <a class="hoverable" href="/Home/ElencoSitiAutorizzati">Società iscritta al n. 1 dell’elenco siti web autorizzati dal Ministero della Giustizia alla pubblicità delle aste giudiziarie - P.D.G. 21/07/2009</a><br /><br />
                    <a class="hoverable" href="http://gestorivenditetelematiche.giustizia.it/PDG_1_1.pdf">Società iscritta al n. 1 del registro gestori vendite telematiche del Ministero della Giustizia - P.D.G. 01/08/2017</a>
                </p>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12">
                <ul class="social-icons">
                    <li><a class="facebook" href="https://it-it.facebook.com/AsteGiudiziarieInlinea/" title="facebook" target="_blank"><em class="icon-facebook"></em></a></li>
                    <li><a class="twitter" href="https://twitter.com/AsteGiudiziari?lang=it" title="twitter" target="_blank"><em class="icon-twitter"></em></a></li>
                    <li><a class="youtube" href="https://www.youtube.com/user/astegiudiziarie" title="youtube" target="_blank"><em class="icon-youtube"></em></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/company/astegiudiziarie" title="Linkedin" target="_blank"><em class="icon-linkedin"></em></a></li>
                </ul>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <div>
                                <a href="http://www.procedure.it"><img class="network" src="/images/loghi/procedure.png" alt="Procedure.it" /></a>
                                <a href="http://www.astetelematiche.it"><img class="network" src="/images/loghi/astetelematiche.png" alt="astetelematiche.it" /></a>
                            </div>
                            <div>
                                <a href="http://www.reteaste.tv"><img class="network" src="/images/loghi/reteaste.png" alt="reteaste.tv" /></a>
                                <a href="http://www.asteentipubblici.it"><img class="network" src="/images/loghi/astetentipubblici.png" alt="asteentipubblici.it" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-6">
                    <a href="/doc/CertificatoISOAGI.pdf" title="link"><img class="isoagi" src="/images/loghi/RINA-ISO9001.png" alt="Certificato ISO AGI" /></a>
                </div>
            </div>
        </div>

        <div class="text-center">
            <ul class="list-inline">
                <li><a class="hoverable" href="/Home/ChiSiamo">Chi siamo</a></li>
                <li><a class="hoverable" href="/Home/LaNostraStoria">La nostra storia</a></li>
                <li><a class="hoverable" href="/Home/INostriClienti">I nostri clienti</a></li>
                <li><a class="hoverable" href="/Home/LavoraConNoi">Lavora con noi</a></li>
                <li><a class="hoverable" href="/Home/Network">Network</a></li>
                <li><a class="hoverable" href="http://www.inlinea.net" title="link" target="_blank">Servizi tecnici: Inlinea S.r.l.</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-xs-12 col-md-6">
                    <div class="copyrights text-left">
                        Copyright 1995-2018 Astegiudiziarie.it
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="copyrights text-right">
                        <a class="hoverable" href="/sitemap">Mappa del sito</a>&#160;|&#160;<a class="hoverable" href="/Home/Privacy">Privacy Policy</a>&#160;|&#160;<a class="hoverable" href="/Home/Qualita">Politica per la qualità</a>&#160;|&#160;<a class="hoverable" href="/Help/Accessibilita">Accessibilità</a>

                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer / End -->
    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#" title="Torna a inizio pagina"></a></div>
</div>
        <div id="cookie-policy" class="text-center hidden nascondi">
    <div class="row">
        <div id="cookie-policy-text" class="col-md-11">
            <p>Questo sito utilizza cookie per funzioni proprie. Se continui nella navigazione o clicchi su un elemento della pagina accetti il loro utilizzo. 
                <a href="/Home/Privacy">Privacy policy</a></p>
            
        </div>
        <div id="btn-cookie-policy" class="col-md-1">
            <span id="btn-accept-text">Accetta</span>
        </div>
    </div>

    
</div>


        <!-- Scripts
        ================================================== -->

        <!-- JQUERY
        ================================================== -->
        <script src="/scripts/jquery-2.2.0.min.js"></script>
<script src="/scripts/jquery-ui.min.js"></script>


        <!-- Scripts
        ================================================== -->
        <script type="text/javascript" src="/Configurazione"></script>

    <script src="/scripts/astegiudiziarie.js"></script>
<script src="/scripts/chosen.min.js"></script>
<script src="/scripts/magnific-popup.min.js"></script>
<script src="/scripts/datepicker-it.js"></script>
<script src="/scripts/owl.carousel.min.js"></script>
<script src="/scripts/sticky-kit.min.js"></script>
<script src="/scripts/slick.min.js"></script>
<script src="/scripts/masonry.min.js"></script>
<script src="/scripts/tooltips.min.js"></script>
<script src="/scripts/jquery.dotdotdot.min.js"></script>
<script src="/scripts/sugar.js"></script>
<script src="/scripts/jRespond.js"></script>
<script src="/scripts/header.js"></script>
<script src="/scripts/accordion.js"></script>
<script src="/scripts/jquery.smartbanner.js"></script>


    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">

        // TODO AG: Serve l'ID della categoria del bene
        // ATTENZIONE AG! A causa del cambio fra tipologie/categorie dovete invertire il criterio di popolamento dei due campi sottostanti
        var idCategoria = 11;
        var idTipologia = 1;

        // TODO AG: Mettere a true se non ha geolocalizzazione
             var noMap = true;

        var WebAPIUrl = "https://webapi.astegiudiziarie.it/api/VenditePrecedenti/1645332/2785584";
    </script>

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="/scripts/mapbox-streets-v9.js"></script>
    <script type="text/javascript" src="/scripts/bootstrap.js"></script>
    <script type="text/javascript" src="/scripts/infobox.min.js"></script>
    <script type="text/javascript" src="/scripts/maps-icons.js"></script>
    <script type="text/javascript" src="/scripts/minimappa-tiles.js"></script>

        <script type="text/javascript" src="/scripts/scheda_del_bene.js"></script>

    

    <!-- Vendor Scripts (Feel free to remove any that you aren't using in your final export) -->
    <script type="text/javascript" src="/scripts/modernizr.custom.js"></script>
    <script type="text/javascript" src="/scripts/jquery.magnific-popup.min.js"></script><!-- Gallery Popup -->
    <script type="text/javascript" src="/scripts/countdown.js"></script><!-- Contact Form -->
    <script type="text/javascript" src="/scripts/astegiudiziarie.js?v=6"></script><!-- Contact Form -->

    <script type="text/javascript" src="/scripts/autenticazione/ajaxProcessEvent.js"></script>

    <script type="text/javascript" src="/scripts/prenotazione-visita-richiesta.js"></script>
    <script type="text/javascript" src="/scripts/prenotazione-visita-ministero.js"></script>
    <script type="text/javascript" src="/scripts/mail-segnalazione-errori.js"></script>
    <script type="text/javascript" src="/scripts/mail-richiesta-informazioni.js"></script>

    <script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>

    <!-- BEGIN SCRIPT SOCIAL -->
    <script type="text/javascript" src="/scripts/social-facebook.js"></script>
    <script type="text/javascript" src="/scripts/social-twitter.js"></script>
    <script type="text/javascript" src="/scripts/social-linkedin.js"></script>
    <!-- END SCRIPT SOCIAL -->





    <script>
        // Report ReteAste
        $(".action-after-login").click(function () {
            // Replicata gestione di A.Malerbi per la login
            var urlRedirect = $(this).data("url-action");

            authUserToken = Cookies.get("RALoggedId");
            if (authUserToken == undefined || authUserToken.length == 0) {
                $("#modal-login #redirectURL").val(urlRedirect);
                $("#modal-login").modal("show");
            } else {
                // Controlliamo la validità dell'authUserToken
                $.ajax({
                    url: baseuri + "api/SOFT/V1/Authenticate/AjaxLogin",
                    type: "GET",
                    headers: { 'AppToken': appToken },
                    data: JSON.stringify({ "AuthUserToken": authUserToken }),
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        if (result && result.TuttoOk == true) {
                            window.location.href = urlRedirect;
                        } else {
                            console.warn(result.Message);
                            clearUserLogged();
                            $("#modal-login #redirectURL").val(urlRedirect);
                            $("#modal-login").modal("show");
                        }
                    },
                    error: function (richiesta, stato, errori) {
                        console.error('Errore durante la chiamata ajax');
                        clearUserLogged();
                        $("#modal-login #redirectURL").val(urlRedirect);
                        $("#modal-login").modal("show");
                    }
                });
            }
        });

        function clearUserLogged() {
            Cookies.remove("RALoggedId");
            loadUserMenu("", "userMenu"); // Call action in userMenu.js
            $("li.userMenuLogged").html("");
        }
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->

    </div>

    
    <script type="text/javascript" src="/scripts/autenticazione/jquery-cookie.js"></script>
    <script type="text/javascript" src="/scripts/autenticazione/loginRA.js"></script>
    <script type="text/javascript" src="/scripts/autenticazione/logoutRA.js"></script>
    <script type="text/javascript" src="/scripts/autenticazione/userMenu.js"></script>
</body>
</html>

