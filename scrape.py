import os
import sys
import requests
import json
import re
import threading

from upload import upload, dbx, in_cloud, get_files_list

import time
import math

from write_csv import create_wb, close_wb, write_row

BASE_URL = "https://www.astegiudiziarie.it"

class Scraper():

	def scrape_profile(self, page_url, ruolo):
		print (page_url)
		response = requests.get('{}{}'.format(BASE_URL,page_url))
		attachments = re.findall(r"(\/Allegato.*?)\"",response.text)
		#print (attachments)
		attachments = list(set(attachments))
		for attachment in attachments:
			if 'Zip' in attachment:
				resp = requests.get('{}{}'.format(BASE_URL,attachment))
				with open('temp.zip','wb') as tempf:
					#resp.raw.decode_content = True
					tempf.write(resp.content) 
				#time.sleep(0.2)

				self.scraped_zips.append(cloud_fn)
				if not in_cloud(dbx, cloud_fn):
					res = upload(dbx, "temp.zip", "aste", cloud_fn)
				return "{}/{}".format("aste", cloud_fn)
				

	def report(self,offer):
		#write json values
		values = [offer.get(key,'') if offer.get(key,'') is not None else '' for key in self.report_columns]
		values = values[0:-1]
		zip_fn = None
		#zip_fn = self.scrape_profile(offer['urlSchedaDettagliata'],offer.get('ruolo',''))
		ruolo_prefix = '-'.join(offer.get('ruolo','').split('-')[-2:])
		cloud_fn = "{}-{}.zip".format(ruolo_prefix, offer['urlSchedaDettagliata'][1:])
		# print(zip_fn)
		if cloud_fn:
			values.append(cloud_fn)
		acc = write_row(values)
		print(acc)

	# def get_whole_list_th(self, city):
	# 	self.start()

	# def run(self):
	# 	self.get_whole_list("")

	def scrape_results(self,offers):
		idLotto_l = [offer['idLotto'] for offer in offers]
		resp = requests.post("https://webapi.astegiudiziarie.it/api//search/data",data=str(idLotto_l),headers = {'content-type': 'application/json'})
		jresp = json.loads(resp.text)
		for offer in jresp:
			self.report(offer)
		# for offer in offers:
		# 	scrape_profile(offer.get('url',None))

	def __init__(self):
		super().__init__()
		self.report_columns = ['dataVendita','dataFineGara','dataUdienza','idLotto','tribunale','riunite','idTipologiaProcedura','esito','idAsta','prezzoBase','note','dataInizioPubblicazione','tipologia','dataInizioGara','categoria','comune','ruolo','annoProcedura','indirizzo','dataFinePubblicazione','provincia','numeroLotto','latitudine','longitudine','urlSchedaDettagliata','idModalitaVendita','descrizione','idTipologia','venditaTelematica','numeroProcedura','idTipologiaVendita','zipdropbox']
		self.scraped_zips = []

	def delete_old_zips(self):
		cloud_zips = get_files_list(dbx)
		old_zips = set(cloud_zips).difference(set(self.scraped_zips))
		for old_zip in old_zips:
			dbx.files_delete("/aste/{}".format(old_zip))
			
	def get_whole_list(self,city):
		idTribunale	= 16
		create_wb()
		write_row(self.report_columns)

		resp = requests.post("https://webapi.astegiudiziarie.it/api//search/map",data='{"latitudineNE":null,"latitudineNW":null,"latitudineSE":null,"latitudineSW":null,"longitudineNE":null,"longitudineNW":null,"longitudineSE":null,"longitudineSW":null,"tipoRicerca":1,"idTipologie":[1,2,3,4,5],"idCategorie":[],"prezzoDa":0,"prezzoA":100000000,"orderBy":"1","idTribunale":16}',headers = {'content-type': 'application/json'})

		jresp = json.loads(resp.text)
		# for offer in jresp:
		# 	offer_url = "".formar(offer['idLotto'])
		chunk_size = 20
		jtest = jresp

		n_chunks = math.ceil(len(jtest)/chunk_size)
		for idx in range(n_chunks):
			chunk = jtest[idx*chunk_size:(idx+1)*chunk_size]
			self.scrape_results(chunk)
		close_wb()
		self.delete_old_zips()

#scrape_profile("https://www.astegiudiziarie.it/vendita-asta-appartamento-carpenedolo-via-lorenzo-forleo-1645332")
#get_whole_list("brescia")
scraper = Scraper()
scraper.get_whole_list("")