import dropbox
import os
import datetime
import time
import contextlib

#dbx = dropbox.Dropbox("zSPLPjKBvtoAAAAAAABCpdTCdZvWyO80jDT8wBwQ-qoTJ63ZO1bA0vAXZfP6lGzj")
dbx = dropbox.Dropbox("wHODYtZnesAAAAAAAAAAC_BiuTKNuHFqRAQSuHpPw-sSXMYZLix_V4EfB4buOzeH")

def in_cloud(dbx,search_fn):
    fns = dbx.files_list_folder("/aste").entries
    for fn in fns:
        if search_fn in fn.name:
            return True           
    return False

def upload(dbx, fullname, folder, name, overwrite=False):
    """Upload a file.
    Return the request response, or None in case of error.
    """
    #path = '/%s/%s/%s' % (folder, subfolder.replace(os.path.sep, '/'), name)
    path = '/%s/%s' % (folder, name)
    while '//' in path:
        path = path.replace('//', '/')
    mode = (dropbox.files.WriteMode.overwrite
            if overwrite
            else dropbox.files.WriteMode.add)
    mtime = os.path.getmtime(fullname)
    with open(fullname, 'rb') as f:
        data = f.read()
    with stopwatch('upload %d bytes' % len(data)):
        try:
            res = dbx.files_upload(
                data, path, mode,
                client_modified=datetime.datetime(*time.gmtime(mtime)[:6]),
                mute=True)
        except dropbox.exceptions.ApiError as err:
            print('*** API error', err)
            return None
    print('uploaded as', res.name.encode('utf8'))
    return res

def delete(dbx,folder):
    path = '/%s' % (folder)
    try:
        res = dbx.files_delete(path)
    except Exception as e:
        print (e)
    return res

def get_files_list(dbx):
    cloud_files = dbx.files_list_folder("/aste").entries
    cloud_fns = [cloud_file.name for cloud_file in cloud_files]
    return cloud_fns

@contextlib.contextmanager
def stopwatch(message):
    """Context manager to print how long a block of code took."""
    t0 = time.time()
    try:
        yield
    finally:
        t1 = time.time()
        print('Total elapsed time for %s: %.3f' % (message, t1 - t0))

#delete(dbx,"top")
#upload(dbx,"/Users/andrea/Documents/projects/python/aunumber/result2", "top","sub","test_result.txt")