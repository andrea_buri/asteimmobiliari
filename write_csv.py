import csv
import json
import sys
import pandas as pd
import os
import xlsxwriter
# reload(syss)


# fout = open(sys.argv[1], 'wb')
# outcsv = csv.writer(fout,delimiter=';')

# outcsv.writerow(jresults[0].keys())


# fout.close()

def append_row(fn,row):
  try:
    df = pd.read_excel(fn,0)
  except:
    df = pd.DataFrame()
  writer = pd.ExcelWriter(fn, engine='xlsxwriter')
  import pdb; pdb.set_trace()
  df = df.append([row])
  df.to_excel(writer, sheet_name="Foglio1",startrow=0, startcol=0, header=False, index=False, encoding = 'utf-8')

  writer.save()

row_count = 0
workbook = None
def create_wb():
    global workbook
    if os.path.exists('report.xlsx'):
        os.remove('report.xlsx')
    workbook = xlsxwriter.Workbook('report.xlsx')
    workbook.add_worksheet('report')

def write_row(row):
    global row_count
    for col_idx, val in enumerate(row):
        workbook.worksheets()[0].write(row_count,col_idx,val)
    row_count += 1
    return row_count
    
def close_wb():
	workbook.close()
create_wb()
