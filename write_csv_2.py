import csv
import json
import sys

reload(sys)
sys.setdefaultencoding('utf8')

results = open('result2').read()
jresults = json.loads(results)

fout = open(sys.argv[1], 'wb')
outcsv = csv.writer(fout,delimiter=';')

outcsv.writerow(jresults[0].keys())

for jres in jresults:
	outcsv.writerow(jres.values())

fout.close()